# Development Reporter

Development reporter is a simple single page HTML report which incorporates development reports;

- Code coverage (nyc JSON Summary)
- Unit tests (Mocha JSON)
- Cucumber features (Cucumber JSON)
- Todos (gulp-todo JSON)

## Installation

To install globally, use your package manager. This package can be installed locally if you wish to use via the API.

```sh
yarn global add git+ssh://deploy@bitbucket.org:depax/depax-reporter.git#master
```

## Usage

TBD

### CLI

TBD

### API

TBD

## Configuration

```json
{
    "brand": "My Company",
    "project": "My Project",
    "reports": []
}
```
