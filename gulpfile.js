/**
 * Provides the generic gulp tasks.
 */

"use strict";

const gulp = require("gulp");
const pkg = require("./package.json");

require("../Devpack")(pkg, gulp);