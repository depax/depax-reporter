/**
 * @todo
 */

"use strict";

const fs = require("fs");
const path = require("path");
const Reporter = require("../dist/Reporter").default;
const configFilename = process.argv[2];

try {
    if (!configFilename) {
        throw new Error("A config filename must be provided.");
    }
    if (!fs.existsSync(configFilename)) {
        throw new Error("Unable to find the provided config file.");
    }

    const reporter = new Reporter(JSON.parse(fs.readFileSync(configFilename)));
    process.chdir(path.dirname(configFilename));
    reporter.render()
        .catch((err) => console.error("[ERROR]", err, "\n"));
} catch (err) {
    console.error("[ERROR]", err.message, "\n");
}