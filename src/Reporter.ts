/**
 * @todo
 */

"use strict";

import chalk = require("chalk");
import fs = require("mz/fs");
import path = require("path");
import pug = require("pug");
import shortid = require("shortid");
import stylus = require("stylus");
import uglifyjs = require("uglify-js");
import uglifycss = require("uglifycss");
import maxToColour from "./Filters/MaxToColour";
import percentageToColour from "./Filters/PercentageToColour";
import valuesToColour from "./Filters/ValuesToColour";
import Renderer from "./Renderers/Renderer";
import Markdown from "./Utils/Markdown";

export const Renderers = {
    "code-coverage": "./Renderers/CodeCoverage",
    "cucumber": "./Renderers/Cucumber",
    "mocha": "./Renderers/Mocha",
    "todos": "./Renderers/Todos",
    "tslint": "./Renderers/TSLint",
    "typedoc": "./Renderers/TypeDoc",
};

export interface IReporterConfig {
    /** @todo */
    output: string;
    /** @todo */
    brand: string;
    /** @todo */
    project: string;
    /** @todo */
    readme?: string;
    /** @todo */
    syntaxTheme?: string;
    /** @todo */
    reports: IReporterConfigReport[];
}

export interface IReporterConfigReport {
    /** @todo */
    renderer: string;
    /** @todo */
    config: any;
    /** @todo */
    weight?: number;
}

export default class Reporter {
    /** @todo */
    protected reports: Renderer[] = [];

    /**
     * @todo
     */
    public constructor(protected config: IReporterConfig) {
        if (this.config.reports) {
            this.config.reports.forEach((renderer) => this.addRenderer(renderer.renderer, renderer.config));
        }
    }

    /**
     * @todo
     */
    public addRenderer(renderer: string, config: any): void {
        if (!Renderers[renderer]) {
            throw new Error(`Attempting to use an unknown renderer called "${renderer}".`);
        }

        const RendererCls: any = require(Renderers[renderer]).default;
        this.reports.push(new RendererCls(this, config));
    }

    /**
     * @todo
     */
    public log(message: string, indent: number = 2, colour: string = "white"): void {
        const args = [];
        if (indent > 0) {
            args.push(Array(indent).join(" ") + ">");
        }

        args.push((chalk as any)[colour](message));
        console.info.apply(console, args);
    }

    /**
     * @todo
     */
    public renderView(view: string, locals: object = {}): Promise<string> {
        const params: any = locals;

        params.maxToColour = maxToColour;
        params.percentageToColour = percentageToColour;
        params.valuesToColour = valuesToColour;

        return new Promise((resolve, reject) => {
            pug.renderFile(view, params, (err, output) => err ? reject(err) : resolve(output));
        });
    }

    /**
     * @todo
     */
    public renderStylus(filename: string): Promise<string> {
        return new Promise((resolve, reject) => {
            fs.readFile(filename)
                .then((input) => {
                    stylus(input.toString())
                        .include(require("nib").path)
                        .set("compress", true)
                        .render((err, css) => err ? reject(err) : resolve(css));
                })
                .catch((err) => reject(err));
        });
    }

    /**
     * @todo
     */
    public async render(): Promise<void> {
        if (this.reports.length === 0) {
            throw new Error("The reporter has not been provided with any report renderers.");
        }

        this.log(`\nGenerating report....\n`, 0, "green");

        const reportCharts = [];
        const renderedPages = [];
        for (const report of this.reports) {
            this.log(`Processing ${(chalk as any).bold(report.rendererId)} report, "${report.title}"...`);
            await report.preprocess();

            this.log("Building charts data...", 4);
            report.charts.forEach((chart) => {
                chart.id = shortid.generate();
                chart.weight = report.weight;
                reportCharts.push(chart);
            });

            this.log("Rendering page data...", 4);
            await report.render();

            const page: any = {};
            page.title = report.title;
            page.id = shortid.generate();
            page.markup = report.markup;
            page.weight = report.weight;

            renderedPages.push(page);
        }

        reportCharts.sort((a, b) => a.weight === b.weight ? 0 : (a.weight < b.weight ? -1 : 1));
        renderedPages.sort((a, b) => a.weight === b.weight ? 0 : (a.weight < b.weight ? -1 : 1));

        this.log("Rendering the report...");
        const markup = await this.renderView(path.normalize(path.join(__dirname, "..", "views", "layout.pug")), {
            brand: this.config.brand || "Reporter",
            charts: reportCharts,
            css: await this.getStyles(),
            js: await this.getScripts(),
            pages: renderedPages,
            project: this.config.project || "Project name",
            readme: await this.getReadmeMarkdown(),
        });

        await fs.writeFile(this.config.output, markup);
        this.log(`\nGenerated report ${this.config.output}.\n`, 0, "green");
    }

    /**
     * @todo
     */
    protected async getStyles(): Promise<string> {
        let css = "";
        if (!this.config.syntaxTheme || [
            "default", "coy", "dark", "funky", "okaidia", "solarizedlight", "tomorrow", "twilight",
        ].indexOf(this.config.syntaxTheme) === -1) {
            this.config.syntaxTheme = "coy";
        }

        this.config.syntaxTheme = this.config.syntaxTheme === "default" ? "prism" : `prism-${this.config.syntaxTheme}`;
        css += (
            await fs.readFile(require.resolve(`prismjs/themes/${this.config.syntaxTheme}.css`))
        ).toString();

        css = uglifycss.processString(css);
        css += await this.renderStylus(path.join(__dirname, "..", "assets", "stylus", "styles.styl"));

        return css;
    }

    /**
     * @todo
     */
    protected async getScripts(): Promise<string> {
        const minified = uglifyjs.minify(
            (await fs.readFile(path.join(__dirname, "..", "assets", "scripts", "script.js"))).toString(),
        );

        if (minified.error) {
            throw minified.error;
        }

        return minified.code;
    }

    /**
     * @todo
     */
    protected async getReadmeMarkdown(): Promise<string> {
        if (this.config.readme) {
            this.log("Parsing the README...");
            return await Markdown(this.config.readme);
        }

        return null;
    }
}
