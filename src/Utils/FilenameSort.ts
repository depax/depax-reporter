/**
 * @todo
 */

"use strict";

import path = require("path");

export default function(key: string = "filename"): (a: any, b: any) => number {
    return (a, b) => {
        const aPath = a[key].split(path.sep);
        const bPath = b[key].split(path.sep);

        if (aPath.length > bPath.length) {
            return -1;
        } else if (aPath.length < bPath.length) {
            return 1;
        }

        for (let i = 0; i < aPath.length; i++) {
            if (aPath[i].toUpperCase() < bPath[i].toUpperCase()) {
                return -1;
            } else if (aPath[i].toUpperCase() > bPath[i].toUpperCase()) {
                return 1;
            } else if (aPath.length < bPath.length) {
                return -1;
            } else if (aPath.length > bPath.length) {
                return 1;
            }
        }

        return 0;
    };
}

/*
(a, b) => {
            const aPath = a.name.split(path.sep);
            const bPath = b.name.split(path.sep);

            if (aPath.length > bPath.length) {
                return -1;
            } else if (aPath.length < bPath.length) {
                return 1;
            }

            for (let i = 0; i < aPath.length; i++) {
                if (aPath[i].toUpperCase() < bPath[i].toUpperCase()) {
                    return -1;
                } else if (aPath[i].toUpperCase() > bPath[i].toUpperCase()) {
                    return 1;
                } else if (aPath.length < bPath.length) {
                    return -1;
                } else if (aPath.length > bPath.length) {
                    return 1;
                }
            }

            return 0;
        }
*/
