/**
 * @todo
 */

"use strict";

import path = require("path");

export const Languages = {
    htm: "html",
    html: "html",
    js: "javascript",
    json: "json",
    php: "php",
    sh: "bash",
    ts: "typescript",
    tsx: "typescript",
};

export default function(filename: string): string {
    return fromExt(path.extname(filename).substr(1));
}

export function fromExt(ext: string): string {
    return Languages[ext] ? Languages[ext] : null;
}
