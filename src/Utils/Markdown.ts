/**
 * @todo
 */

"use strict";

import fs = require("mz/fs");
import prism = require("node-prismjs");
import showdown = require("showdown");
import { fromExt } from "./CodeLanguages";

const showdownExtensions = [];
showdownExtensions.push(() => {
    return {
        filter: (text, converter, options) => {
            return text
                .replace(/<pre><code class="(.*) language-.*">([\s\S]*?)<\/code><\/pre>/g, (pattern, lang, code) => {
                    const cl = fromExt(lang);
                    if (cl) {
                        code = prism.highlight(code, prism.languages[cl], cl);

                        const lines = code.replace(/\/r\/n/g, "\n").split("\n");
                        let lineCode = "<span aria-hidden=\"true\" class=\"line-numbers\">";
                        for (let i = 0, len = lines.length - 1; i < len; i++) {
                            lineCode += "<i></i>";
                        }
                        lineCode += "</span>";

                        code = lineCode + code;
                    }

                    return `<pre class="code language-${lang}">${code}</pre>`;
                });
        },
        type: "output",
    };
});

export default async function(filename: string): Promise<string> {
    const contents = (await fs.readFile(filename)).toString();
    const converter = new showdown.Converter({ extensions: showdownExtensions });
    return converter.makeHtml(contents);
}
