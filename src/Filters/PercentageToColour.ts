/**
 * @todo
 */

"use strict";

export default function(value, redMax, orangeMax) {
    value = parseInt(value, 10);
    if (value <= redMax) {
        return "red";
    } else if (value < orangeMax) {
        return "orange";
    } else {
        return "green";
    }
}
