/**
 * @todo Define a file docblock.
 */

"use strict";

/**
 * A filter function which will provide a colour based upon the given values.
 *
 * @param value The value.
 * @param redMax If the value is this or higher, then the colour returned would be red.
 * @param orangeMax If the value is higher than this (and lower than redMax) then the colour returned would be orange.
 *
 * @return Returns the colour classname.
 */
export default function(value, redMax, orangeMax) {
    // @fixme Make this a little cleaner.
    if (value >= redMax) {
        return "red";
    } else if (value > orangeMax) {
        return "orange";
    } else {
        return "green";
    }
}
