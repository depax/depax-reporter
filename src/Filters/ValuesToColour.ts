/**
 * @todo
 */

"use strict";

import percentageToColour from "./PercentageToColour";

export default function(value, total, redMax, orangeMax) {
    const percentage = value === total ? 100 : (value === 0 ? 0 : ((100 * value) / total).toFixed(2));
    return percentageToColour(percentage, redMax, orangeMax);
}
