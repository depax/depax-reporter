/**
 * @todo
 */

"use strict";

import fs = require("mz/fs");
import path = require("path");
import Reporter from "../Reporter";
import FilenameSort from "../Utils/FilenameSort";
import Renderer, { IChart } from "./Renderer";

export interface ITslintConfig {
    /** @todo */
    title?: string;
    /** @todo */
    filename: string;
    /** @todo */
    chart?: boolean;
    /** @todo */
    view?: string;
    /** @todo */
    levels?: {
        /** @todo */
        red: number;
        /** @todo */
        orange: number;
    };
}

export interface ITslintReport {
    /** @todo */
    filename: string;
    /** @todo */
    totals: {
        /** @todo */
        total: number;
        /** @todo */
        errors: number;
        /** @todo */
        warnings: number;
    };
    /** @todo */
    files: ITslintReportFile[];
}

export interface ITslintReportFile {
    /** @todo */
    filename: string;
    /** @todo */
    items: ITslintReportItem[];
}

export interface ITslintReportItem {
    /** @todo */
    line: ITslintReportItemLine;
    /** @todo */
    rule: string;
    /** @todo */
    message: string;
    /** @todo */
    severity: string;
}

export interface ITslintReportItemLine {
    /** @todo */
    line: number;
    /** @todo */
    character: number;
}

export default class TSLint extends Renderer {
    /** @todo */
    protected config: ITslintConfig;

    /** @todo */
    protected report: ITslintReport;

    /** @inheritdoc */
    public constructor(reporter: Reporter, config: ITslintConfig | string) {
        super(reporter, null);
        this.config = typeof config === "string" ? { filename: config } : config;
    }

    /** @inheritdoc */
    public get rendererId(): string { return "tslint"; }

    /** @inheritdoc */
    public get title(): string { return this.config.title || "TSLint"; }

    /** @inheritdoc */
    public get charts(): IChart[] {
        if (this.config.chart === false || this.report.totals.total === 0) {
            return [];
        }

        const chart = {
            rows: [],
            title: this.title,
        };

        if (this.report.totals.warnings === 0) {
            chart.rows.push({ label: "Errors", value: this.report.totals.errors });
            chart.rows.push({ label: "Warnings", value: this.report.totals.warnings });
        } else {
            chart.rows.push({ label: "Warnings", value: this.report.totals.warnings });
            chart.rows.push({ label: "Errors", value: this.report.totals.errors });
        }

        return [ chart ];
    }

    /** @inheritdoc */
    public get generatedReport(): any { return this.report; }

    /** @inheritdoc */
    public async preprocess(): Promise<void> {
        const reportFileContents = (await fs.readFile(this.config.filename)).toString();
        const reportFile = JSON.parse(reportFileContents);

        this.report = {
            filename: this.config.filename,
            files: [],
            totals: {
                errors: reportFile.totals.errors,
                total: reportFile.totals.errors + reportFile.totals.warnings,
                warnings: reportFile.totals.warnings,
            },
        };

        reportFile.items.forEach((failure) => {
            let idx;
            for (let i = 0, len = this.report.files.length; i < len; i++) {
                if (this.report.files[i].filename === failure.filename) {
                    idx = i;
                    break;
                }
            }

            if (idx === undefined) {
                idx = this.report.files.push({ filename: failure.filename, items: [] }) - 1;
            }

            this.report.files[idx].items.push({
                line: failure.line,
                message: failure.message,
                rule: failure.ruleName,
                severity: failure.severity,
            });
        });

        this.report.files.sort(FilenameSort("filename"));
    }

    /** @inheritdoc */
    protected async doRender(): Promise<string> {
        const cfg: any = {};
        cfg.redMax = this.config.levels && this.config.levels.red ?
            this.config.levels.red || 20 :
            20;

        cfg.orangeMax = this.config.levels && this.config.levels.orange ?
            this.config.levels.orange || 0 :
            0;

        return await this.reporter.renderView(
            this.config.view || path.normalize(path.join(__dirname, "..", "..", "views", "TSLint", "layout.pug")),
            { config: cfg, report: this.report },
        );
    }
}
