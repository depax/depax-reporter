/**
 * @todo
 */

"use strict";

import fs = require("mz/fs");
import path = require("path");
import shortid = require("shortid");
import Reporter from "../Reporter";
import Renderer, { IChart } from "./Renderer";

export interface IUnitTestConfig {
    /** @todo */
    title?: string;
    /** @todo */
    filename: string;
    /** @todo */
    chart?: boolean | IUnitTestConfigCharts;
    /** @todo */
    view?: string;
}

export interface IUnitTestConfigCharts {
    /** @todo */
    overall?: boolean;
    /** @todo */
    suites?: boolean;
    /** @todo */
    suitesLabel?: string;
}

export interface IUnitTestReport {
    /** @todo */
    filename: string;
    /** @todo */
    totals: {
        /** @todo */
        suites: number;
        /** @todo */
        tests: number;
        /** @todo */
        total: number;
        /** @todo */
        passed: number;
        /** @todo */
        failed: number;
        /** @todo */
        pending: number;
        /** @todo */
        skipped: number;
    };
    /** @todo */
    duration: number;
    /** @todo */
    suites: IUnitTestReportSuite[];
}

export interface IUnitTestReportSuite {
    /** @todo */
    parent?: IUnitTestReportSuite;
    /** @todo */
    id: string;
    /** @todo */
    name: string;
    /** @todo */
    file: string;
    /** @todo */
    stats: {
        /** @todo */
        total: number;
        /** @todo */
        passed: number;
        /** @todo */
        failed: number;
        /** @todo */
        pending: number;
        /** @todo */
        skipped: number;
    };
    /** @todo */
    suites: IUnitTestReportSuite[];
    /** @todo */
    tests: IUnitTestReportTest[];
}

export interface IUnitTestReportTest {
    /** @todo */
    name: string;
    /** @todo */
    timedout: boolean;
    /** @todo */
    duration: number;
    /** @todo */
    status: string;
}

export default class Mocha extends Renderer {
    /** @todo */
    protected config: IUnitTestConfig;

    /** @todo */
    protected report: IUnitTestReport;

    /** @inheritdoc */
    public constructor(reporter: Reporter, config: IUnitTestConfig | string) {
        super(reporter, null);
        this.config = typeof config === "string" ? { filename: config } : config;
    }

    /** @inheritdoc */
    public get rendererId(): string { return "mocha"; }

    /** @inheritdoc */
    public get title(): string { return this.config.title || "Test`s"; }

    /** @inheritdoc */
    public get charts(): IChart[] {
        const overall: boolean = this.config.chart === undefined ||
            this.config.chart === true ||
            (this.config.chart as IUnitTestConfigCharts).overall === undefined ||
            (this.config.chart as IUnitTestConfigCharts).overall === true;

        const suites: boolean = this.config.chart === undefined ||
            this.config.chart === true ||
            (this.config.chart as IUnitTestConfigCharts).suites === undefined ||
            (this.config.chart as IUnitTestConfigCharts).suites === true;

        const charts = [];

        if (overall) {
            charts.push({
                colors: [ "#5cb85c", "#d9534f", "#999", "#f0ad4e" ],
                rows: [
                    { label: "Passed", value: this.report.totals.passed },
                    { label: "Failed", value: this.report.totals.failed },
                    { label: "Pending", value: this.report.totals.pending },
                    { label: "Skipped", value: this.report.totals.skipped },
                ],
                title: this.title,
            });
        }

        if (suites) {
            charts.push({
                rows: [
                    { label: "Suites", value: this.report.totals.suites },
                    { label: "Tests", value: this.report.totals.tests },
                ],
                title: this.config.chart && (this.config.chart as IUnitTestConfigCharts).suitesLabel ?
                    (this.config.chart as IUnitTestConfigCharts).suitesLabel :
                    `${this.title} (Suites)`,
            });
        }

        return charts;
    }

    /** @inheritdoc */
    public get generatedReport(): any { return this.report; }

    /** @inheritdoc */
    public async preprocess(): Promise<void> {
        const reportFileContents = (await fs.readFile(this.config.filename)).toString();
        const reportFile = JSON.parse(reportFileContents);

        this.report = {
            duration: reportFile.stats.duration,
            filename: this.config.filename,
            suites: [],
            totals: {
                failed: reportFile.stats.failures,
                passed: reportFile.stats.passes,
                pending: reportFile.stats.pending,
                skipped: reportFile.stats.skipped,
                suites: 0,
                tests: 0,
                total: reportFile.stats.tests,
            },
        };

        reportFile.suites.suites.forEach((item) => {
            const suite = this.processSuite(item, null);
            if (suite) {
                this.report.suites.push(suite);
            }
        });
        this.report.suites.sort((a, b) => a.name.localeCompare(b.name));
    }

    /**
     * @todo
     */
    protected processSuite(item, parent: IUnitTestReportSuite): IUnitTestReportSuite {
        this.report.totals.suites++;
        const suite: IUnitTestReportSuite = {
            file: item.file.substr(1),
            id: shortid.generate(),
            name: item.title,
            stats: {
                failed: item.totalFailures,
                passed: item.totalPasses,
                pending: item.totalPending,
                skipped: item.totalSkipped,
                total: item.totalTests,
            },
            suites: [],
            tests: [],
        };

        if (parent) {
            suite.parent = parent;
        }

        if (item.suites.length) {
            item.suites.forEach((subitem) => {
                const subsuite = this.processSuite(subitem, suite);
                if (subsuite) {
                    suite.suites.push(subsuite);
                }
            });
        }

        if (item.tests.length) {
            item.tests.forEach((subitem) => suite.tests.push(this.processTest(subitem, suite)));
        }

        if (suite.parent) {
            Object.keys(suite.stats).forEach((stat) => suite.parent.stats[stat] += suite.stats[stat]);
        }

        return suite;
    }

    /**
     * @todo
     */
    protected processTest(item, suite: IUnitTestReportSuite): IUnitTestReportTest {
        this.report.totals.tests++;
        return {
            duration: item.duration,
            name: item.title,
            status: item.state,
            timedout: item.timedOut,
        };
    }

    /** @inheritdoc */
    protected async doRender(): Promise<string> {
        return await this.reporter.renderView(
            this.config.view || path.normalize(path.join(__dirname, "..", "..", "views", "UnitTest", "layout.pug")),
            { report: this.report },
        );
    }
}
