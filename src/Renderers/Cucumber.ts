/**
 * @todo
 */

"use strict";

import fs = require("mz/fs");
import path = require("path");
import shortid = require("shortid");
import Reporter from "../Reporter";
import Renderer, { IChart } from "./Renderer";

export interface ICucumberConfig {
    /** @todo */
    title?: string;
    /** @todo */
    filename: string;
    /** @todo */
    chart?: boolean | ICucumberConfigCharts;
    /** @todo */
    view?: string;
}

export interface ICucumberConfigCharts {
    /** @todo */
    overall?: boolean;
    /** @todo */
    features?: boolean;
    /** @todo */
    featuresLabel?: string;
}

export interface ICucumberReport {
    /** @todo */
    filename: string;
    /** @todo */
    totals: {
        /** @todo */
        features: number;
        /** @todo */
        scenarios: number;
        /** @todo */
        steps: number;
        /** @todo */
        total: number;
        /** @todo */
        passed: number;
        /** @todo */
        failed: number;
        /** @todo */
        pending: number;
        /** @todo */
        notdefined: number;
        /** @todo */
        ambiguous: number;
        /** @todo */
        skipped: number;
    };
    /** @todo */
    duration: number;
    /** @todo */
    features: ICucumberReportFeature[];
}

export interface ICucumberReportFeature {
    /** @todo */
    id: string;
    /** @todo */
    name: string;
    /** @todo */
    keyword: string;
    /** @todo */
    tags: string[];
    /** @todo */
    stats: {
        /** @todo */
        total: number;
        /** @todo */
        passed: number;
    };
    /** @todo */
    scenarios: ICucumberReportScenario[];
}

export interface ICucumberReportScenario {
    /** @todo */
    keyword: string;
    /** @todo */
    name: string;
    /** @todo */
    stats: {
        /** @todo */
        total: number;
        /** @todo */
        passed: number;
        /** @todo */
        failed: number;
        /** @todo */
        pending: number;
        /** @todo */
        notdefined: number;
        /** @todo */
        ambiguous: number;
        /** @todo */
        skipped: number;
    };
    /** @todo */
    steps: ICucumberReportStep[];
}

export interface ICucumberReportStep {
    /** @todo */
    keyword: string;
    /** @todo */
    name: string;
    /** @todo */
    status: string;
    /** @todo */
    duration: number;
}

export default class Cucumber extends Renderer {
    /** @todo */
    protected config: ICucumberConfig;

    /** @todo */
    protected report: ICucumberReport;

    /** @inheritdoc */
    public constructor(reporter: Reporter, config: ICucumberConfig | string) {
        super(reporter, null);
        this.config = typeof config === "string" ? { filename: config } : config;
    }

    /** @inheritdoc */
    public get rendererId(): string { return "cucumber"; }

    /** @inheritdoc */
    public get title(): string { return this.config.title || "Cucumber"; }

    /** @inheritdoc */
    public get charts(): IChart[] {
        const overall: boolean = this.config.chart === undefined ||
            this.config.chart === true ||
            (this.config.chart as ICucumberConfigCharts).overall === undefined ||
            (this.config.chart as ICucumberConfigCharts).overall === true;

        const features: boolean = this.config.chart === undefined ||
            this.config.chart === true ||
            (this.config.chart as ICucumberConfigCharts).features === undefined ||
            (this.config.chart as ICucumberConfigCharts).features === true;

        const charts = [];

        if (overall) {
            charts.push({
                colors: [ "#5cb85c", "#d9534f", "#999", "#5bc0de", "#428bca", "#f0ad4e" ],
                rows: [
                    { label: "Passed", value: this.report.totals.passed },
                    { label: "Failed", value: this.report.totals.failed },
                    { label: "Pending", value: this.report.totals.pending },
                    { label: "Undefined", value: this.report.totals.notdefined },
                    { label: "Ambiguous", value: this.report.totals.ambiguous },
                    { label: "Skipped", value: this.report.totals.skipped },
                ],
                title: this.title,
            });
        }

        if (features) {
            charts.push({
                rows: [
                    { label: "Features", value: this.report.totals.features },
                    { label: "Scenarios", value: this.report.totals.scenarios },
                    { label: "Steps", value: this.report.totals.steps },
                ],
                title: this.config.chart && (this.config.chart as ICucumberConfigCharts).featuresLabel ?
                    (this.config.chart as ICucumberConfigCharts).featuresLabel :
                    `${this.title} (Features)`,
            });
        }

        return charts;
    }

    /** @inheritdoc */
    public get generatedReport(): any { return this.report; }

    /** @inheritdoc */
    public async preprocess(): Promise<void> {
        const reportFileContents = (await fs.readFile(this.config.filename)).toString();
        const reportFile = JSON.parse(reportFileContents);

        this.report = {
            duration: 0,
            features: [],
            filename: this.config.filename,
            totals: {
                ambiguous: 0,
                failed: 0,
                features: 0,
                notdefined: 0,
                passed: 0,
                pending: 0,
                scenarios: 0,
                skipped: 0,
                steps: 0,
                total: 0,
            },
        };

        reportFile.forEach((item) => this.report.features.push(this.processFeature(item)));
        this.report.duration = this.report.duration / 1000000;
        this.report.features.sort((a, b) => a.name.localeCompare(b.name));
    }

    /**
     * @todo
     */
    protected processFeature(item): ICucumberReportFeature {
        this.report.totals.features++;
        const feature = {
            id: shortid.generate(),
            keyword: item.keyword,
            name: item.name,
            scenarios: [],
            stats: {
                passed: 0,
                total: 0,
            },
            tags: [],
        };

        item.tags.forEach((tag) => feature.tags.push(tag.name.trim()));
        item.elements.forEach((subitem) => feature.scenarios.push(this.processScenario(subitem, feature)));

        return feature;
    }

    /**
     * @todo
     */
    protected processScenario(item, feature: ICucumberReportFeature): ICucumberReportScenario {
        this.report.totals.scenarios++;
        const scenario = {
            keyword: item.keyword,
            name: item.name,
            stats: {
                ambiguous: 0,
                failed: 0,
                notdefined: 0,
                passed: 0,
                pending: 0,
                skipped: 0,
                total: 0,
            },
            steps: [],
        };

        item.steps.forEach((subitem) => {
            const step = this.processStep(subitem, feature, scenario);
            if (step) {
                this.report.totals.steps++;

                scenario.stats.total += 1;
                scenario.stats[step.status] += 1;

                this.report.duration += subitem.result.duration ? parseInt(subitem.result.duration, 10) : 0;
                this.report.totals.total += 1;
                this.report.totals[step.status] += 1;

                feature.stats.total += 1;
                if (step.status === "passed") {
                    feature.stats.passed += 1;
                }

                scenario.steps.push(step);
            }
        });

        return scenario;
    }

    /**
     * @todo
     */
    protected processStep(
        item,
        feature: ICucumberReportFeature,
        scenario: ICucumberReportScenario,
    ): ICucumberReportStep {
        return item.hidden ? null : {
            duration: item.result.duration ? item.result.duration / 1000000 : 0,
            keyword: item.keyword.trim(),
            name: item.name,
            status: item.result.status,
        };
    }

    /** @inheritdoc */
    protected async doRender(): Promise<string> {
        return await this.reporter.renderView(
            this.config.view || path.normalize(path.join(__dirname, "..", "..", "views", "Cucumber", "layout.pug")),
            { report: this.report },
        );
    }
}
