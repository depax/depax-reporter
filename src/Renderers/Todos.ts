/**
 * @todo
 */

"use strict";

import fs = require("mz/fs");
import path = require("path");
import Reporter from "../Reporter";
import FilenameSort from "../Utils/FilenameSort";
import Renderer, { IChart } from "./Renderer";

export interface ITodosConfig {
    /** @todo */
    title?: string;
    /** @todo */
    filename: string;
    /** @todo */
    chart?: boolean;
    /** @todo */
    view?: string;
    /** @todo */
    levels?: {
        /** @todo */
        red: number;
        /** @todo */
        orange: number;
    };
}

export interface ITodosReport {
    /** @todo */
    filename: string;
    /** @todo */
    totals: {
        /** @todo */
        total: number;
        /** @todo */
        todo: number;
        /** @todo */
        fixme: number;
    };
    /** @todo */
    files: ITodosReportFile[];
}

export interface ITodosReportFile {
    /** @todo */
    filename: string;
    /** @todo */
    items: ITodosReportFileItem[];
}

export interface ITodosReportFileItem {
    /** @todo */
    type: string;
    /** @todo */
    line: string;
    /** @todo */
    text: string;
}

export default class Todos extends Renderer {
    /** @todo */
    protected config: ITodosConfig;

    /** @todo */
    protected report: ITodosReport;

    /** @inheritdoc */
    public constructor(reporter: Reporter, config: ITodosConfig | string) {
        super(reporter, null);
        this.config = typeof config === "string" ? { filename: config } : config;
    }

    /** @inheritdoc */
    public get rendererId(): string { return "todos"; }

    /** @inheritdoc */
    public get title(): string { return this.config.title || "Todo`s"; }

    /** @inheritdoc */
    public get charts(): IChart[] {
        if (this.config.chart === false || this.report.totals.total === 0) {
            return [];
        }

        const chart = {
            rows: [
                { label: "Todo", value: this.report.totals.todo },
                { label: "Fixme", value: this.report.totals.fixme },
            ],
            title: this.title,
        };

        return [ chart ];
    }

    /** @inheritdoc */
    public get generatedReport(): any { return this.report; }

    /** @inheritdoc */
    public async preprocess(): Promise<void> {
        const reportFileContents = (await fs.readFile(this.config.filename)).toString();
        const reportFile = JSON.parse(reportFileContents);

        this.report = {
            filename: this.config.filename,
            files: [],
            totals: {
                fixme: 0,
                todo: 0,
                total: 0,
            },
        };

        reportFile.forEach((item) => {
            let idx = this.report.files.findIndex((e) => e.filename === item.file);
            if (idx === -1) {
                idx = this.report.files.push({
                    filename: item.file,
                    items: [],
                }) - 1;
            }

            this.report.totals.total++;
            this.report.totals[item.kind.toLowerCase()]++;

            this.report.files[idx].items.push({
                line: item.line,
                text: item.text,
                type: item.kind,
            });
        });

        this.report.files.sort(FilenameSort("filename"));
    }

    /** @inheritdoc */
    protected async doRender(): Promise<string> {
        const cfg: any = {};
        cfg.redMax = this.config.levels && this.config.levels.red ?
            this.config.levels.red || 20 :
            20;

        cfg.orangeMax = this.config.levels && this.config.levels.orange ?
            this.config.levels.orange || 0 :
            0;

        return await this.reporter.renderView(
            this.config.view || path.normalize(path.join(__dirname, "..", "..", "views", "Todos", "layout.pug")),
            { config: cfg, report: this.report },
        );
    }
}
