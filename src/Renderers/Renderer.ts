/**
 * @todo
 */

"use strict";

import Reporter from "../Reporter";

export interface IChartRow {
    /** @todo */
    label: string;
    /** @todo */
    value: number;
}

export interface IChart {
    /** @todo */
    id?: string;
    /** @todo */
    weight?: number;
    /** @todo */
    title: string;
    /** @todo */
    rows: IChartRow[];
    /** @todo */
    colours?: string[];
}

export default abstract class Renderer {
    /** @todo */
    protected output: string;

    /**
     * @todo
     */
    public constructor(protected reporter: Reporter, protected config: any) {}

    /** @todo */
    public abstract get rendererId(): string;

    /** @todo */
    public abstract get title(): string;

    /** @todo */
    public get weight(): number { return this.config.weight || 0; }
    public set weight(value: number) { this.config.weight = value; }

    /** @todo */
    public abstract get charts(): IChart[];

    /** @todo */
    public get markup(): string { return this.output; }

    /** @todo */
    public abstract get generatedReport(): any;

    /**
     * @todo
     */
    public abstract async preprocess(): Promise<void>;

    /**
     * @todo
     */
    public async render(): Promise<void> { this.output = await this.doRender(); }

    /**
     * @todo
     */
    protected abstract async doRender(): Promise<string>;
}
