/**
 * @todo
 */

"use strict";

import fs = require("mz/fs");
import prism = require("node-prismjs");
import path = require("path");
import shortid = require("shortid");
import Reporter from "../Reporter";
import CodeLanguages from "../Utils/CodeLanguages";
import FilenameSort from "../Utils/FilenameSort";
import Renderer, { IChart } from "./Renderer";

export interface ICodeCoverageConfig {
    /** @todo */
    title?: string;
    /** @todo */
    filename: string;
    /** @todo */
    chart?: boolean | ICodeCoverageConfigCharts;
    /** @todo */
    view?: string;
    /** @todo */
    pctLevels?: {
        /** @todo */
        passedMax?: number;
        /** @todo */
        failedMax?: number;
    };
}

export interface ICodeCoverageConfigCharts {
    /** @todo */
    overall?: boolean;
    /** @todo */
    statements?: boolean;
    /** @todo */
    statementsLabel?: string;
    /** @todo */
    branches?: boolean;
    /** @todo */
    branchesLabel?: string;
    /** @todo */
    functions?: boolean;
    /** @todo */
    functionsLabel?: string;
    /** @todo */
    lines?: boolean;
    /** @todo */
    linesLabel?: string;
}

export interface ICodeCoverageReport {
    /** @todo */
    filename: string;
    /** @todo */
    totals: {
        /** @todo */
        percentage: number | string;
        /** @todo */
        statements: ICodeCoverageTotals;
        /** @todo */
        branches: ICodeCoverageTotals;
        /** @todo */
        functions: ICodeCoverageTotals;
        /** @todo */
        lines: ICodeCoverageTotals;
    };
    /** @todo */
    files: ICodeCoverageFile[];
}

export interface ICodeCoverageTotals {
    /** @todo */
    total: number;
    /** @todo */
    done: number;
}

export interface ICodeCoverageFile {
    /** @todo */
    filename: string;
    /** @todo */
    name: string;
    /** @todo */
    language: string;
    /** @todo */
    stats: {
        /** @todo */
        percentage: number | string;
        /** @todo */
        statements: ICodeCoverageTotals;
        /** @todo */
        branches: ICodeCoverageTotals;
        /** @todo */
        functions: ICodeCoverageTotals;
        /** @todo */
        lines: ICodeCoverageTotals;
    };
    /** @todo */
    code: string;
    /** @todo */
    noLines: number;
}

export interface ICodeCoverageTokenLine {
    /** @todo */
    lineNo: number;
    /** @todo */
    tokens: ICodeCoverageToken[];
}

export interface ICodeCoverageToken {
    /** @todo */
    column: number;
    /** @todo */
    tag: string;
}

export default class CodeCoverage extends Renderer {
    /** @todo */
    protected config: ICodeCoverageConfig;

    /** @todo */
    protected report: ICodeCoverageReport;

    /** @todo */
    protected tokens: ICodeCoverageTokenLine[];

    /** @inheritdoc */
    public constructor(reporter: Reporter, config: ICodeCoverageConfig | string) {
        super(reporter, null);
        this.config = typeof config === "string" ? { filename: config } : config;
    }

    /** @inheritdoc */
    public get rendererId(): string { return "code-coverage"; }

    /** @inheritdoc */
    public get title(): string { return this.config.title || "Coverage"; }

    /** @inheritdoc */
    public get charts(): IChart[] {
        const overall: boolean = this.config.chart === undefined ||
            this.config.chart === true ||
            (this.config.chart as ICodeCoverageConfigCharts).overall === undefined ||
            (this.config.chart as ICodeCoverageConfigCharts).overall === true;

        const statements: boolean = this.config.chart === undefined ||
            this.config.chart === true ||
            (this.config.chart as ICodeCoverageConfigCharts).statements === undefined ||
            (this.config.chart as ICodeCoverageConfigCharts).statements === true;

        const branches: boolean = this.config.chart === undefined ||
            this.config.chart === true ||
            (this.config.chart as ICodeCoverageConfigCharts).branches === undefined ||
            (this.config.chart as ICodeCoverageConfigCharts).branches === true;

        const functions: boolean = this.config.chart === undefined ||
            this.config.chart === true ||
            (this.config.chart as ICodeCoverageConfigCharts).functions === undefined ||
            (this.config.chart as ICodeCoverageConfigCharts).functions === true;

        const lines: boolean = this.config.chart === undefined ||
            this.config.chart === true ||
            (this.config.chart as ICodeCoverageConfigCharts).lines === undefined ||
            (this.config.chart as ICodeCoverageConfigCharts).lines === true;

        const charts = [];

        if (overall) {
            charts.push({
                rows: [
                    { label: "Statements", value: this.report.totals.statements.total },
                    { label: "Branches", value: this.report.totals.branches.total },
                    { label: "Functions", value: this.report.totals.functions.total },
                    { label: "Lines", value: this.report.totals.lines.total },
                ],
                title: this.title,
            });
        }

        if (statements) {
            charts.push({
                colors: [ "#5cb85c", "#d9534f" ],
                rows: [
                    { label: "Done", value: this.report.totals.statements.done },
                    {
                        label: "Incomplete",
                        value: this.report.totals.statements.total - this.report.totals.statements.done,
                    },
                ],
                title: this.config.chart && (this.config.chart as ICodeCoverageConfigCharts).statementsLabel ?
                    (this.config.chart as ICodeCoverageConfigCharts).statementsLabel :
                    `${this.title} (Statements)`,
            });
        }

        if (branches) {
            charts.push({
                colors: [ "#5cb85c", "#d9534f" ],
                rows: [
                    { label: "Done", value: this.report.totals.branches.done },
                    {
                        label: "Incomplete",
                        value: this.report.totals.branches.total - this.report.totals.branches.done,
                    },
                ],
                title: this.config.chart && (this.config.chart as ICodeCoverageConfigCharts).branchesLabel ?
                    (this.config.chart as ICodeCoverageConfigCharts).branchesLabel :
                    `${this.title} (Branches)`,
            });
        }

        if (functions) {
            charts.push({
                colors: [ "#5cb85c", "#d9534f" ],
                rows: [
                    { label: "Done", value: this.report.totals.functions.done },
                    {
                        label: "Incomplete",
                        value: this.report.totals.functions.total - this.report.totals.functions.done,
                    },
                ],
                title: this.config.chart && (this.config.chart as ICodeCoverageConfigCharts).functionsLabel ?
                    (this.config.chart as ICodeCoverageConfigCharts).functionsLabel :
                    `${this.title} (Functions)`,
            });
        }

        if (lines) {
            charts.push({
                colors: [ "#5cb85c", "#d9534f" ],
                rows: [
                    { label: "Done", value: this.report.totals.lines.done },
                    {
                        label: "Incomplete",
                        value: this.report.totals.lines.total - this.report.totals.lines.done,
                    },
                ],
                title: this.config.chart && (this.config.chart as ICodeCoverageConfigCharts).linesLabel ?
                    (this.config.chart as ICodeCoverageConfigCharts).linesLabel :
                    `${this.title} (Lines)`,
            });
        }

        return charts;
    }

    /** @inheritdoc */
    public get generatedReport(): any { return this.report; }

    /** @inheritdoc */
    public async preprocess(): Promise<void> {
        const reportFileContents = (await fs.readFile(this.config.filename)).toString();
        const reportFile = JSON.parse(reportFileContents);

        this.report = {
            filename: this.config.filename,
            files: [],
            totals: {
                branches: { total: 0, done: 0 },
                functions: { total: 0, done: 0 },
                lines: { total: 0, done: 0 },
                percentage: 0,
                statements: { total: 0, done: 0 },
            },
        };

        const filenames = Object.keys(reportFile);
        const basedir = this.workoutBaseDir(filenames);

        for (const filename of filenames) {
            const file = await this.processFile(filename, reportFile[filename], basedir);
            this.report.files.push(file);
        }

        this.report.totals.percentage = ((100 * (
            this.report.totals.statements.done +
            this.report.totals.branches.done +
            this.report.totals.functions.done +
            this.report.totals.lines.done
        )) / (
            this.report.totals.statements.total +
            this.report.totals.branches.total +
            this.report.totals.functions.total +
            this.report.totals.lines.total
        )).toFixed(2);

        this.report.files.sort(FilenameSort("name"));
    }

    /**
     * @todo
     */
    protected workoutBaseDir(filenames: string[]): string {
        let basedir = path.dirname(filenames[0]);
        filenames.forEach((filename) => {
            while (filename.indexOf(basedir) !== 0) {
                basedir = path.dirname(basedir);
            }
        });

        return basedir;
    }

    /**
     * @todo
     */
    protected async processFile(fullFilename: string, item, basedir: string): Promise<ICodeCoverageFile> {
        const contents = (await fs.readFile(fullFilename)).toString();
        const file: ICodeCoverageFile = {
            code: "",
            filename: fullFilename,
            language: "",
            name: path.relative(basedir, fullFilename),
            noLines: 0,
            stats: {
                branches: { total: 0, done: 0 },
                functions: { total: 0, done: 0 },
                lines: { total: 0, done: 0 },
                percentage: 0,
                statements: { total: 0, done: 0 },
            },
        };

        this.tokens = [];
        const lines = {};
        const doneLines = {};

        Object.keys(item.s).forEach((index) => {
            lines[item.statementMap[index].start.line] = true;
            if (this.processStatement(
                item.statementMap[index],
                index,
                item.s[index],
                file,
            )) {
                doneLines[item.statementMap[index].start.line] = true;
            }
        });

        Object.keys(item.b).forEach((index) => this.processBranch(
            item.branchMap[index],
            index,
            item.b[index],
            file,
        ));

        Object.keys(item.f).forEach((index) => this.processFunction(
            item.fnMap[index],
            index,
            item.f[index],
            file,
        ));

        this.report.totals.statements.total += file.stats.statements.total;
        this.report.totals.statements.done += file.stats.statements.done;
        this.report.totals.branches.total += file.stats.branches.total;
        this.report.totals.branches.done += file.stats.branches.done;
        this.report.totals.functions.total += file.stats.functions.total;
        this.report.totals.functions.done += file.stats.functions.done;

        file.stats.lines.total = (Object as any).values(lines).length;
        file.stats.lines.done = (Object as any).values(doneLines).length;
        this.report.totals.lines.total += file.stats.lines.total;
        this.report.totals.lines.done += file.stats.lines.done;

        file.stats.percentage = ((100 * (
            file.stats.statements.done +
            file.stats.branches.done +
            file.stats.functions.done +
            file.stats.lines.done
        )) / (
            file.stats.statements.total +
            file.stats.branches.total +
            file.stats.functions.total +
            file.stats.lines.total
        )).toFixed(2);

        const rawLines = contents.replace(/\r\n/g, "/n").split("\n");
        const lineNos = this.tokens.sort((a, b) => a.lineNo === b.lineNo ? 0 : (a.lineNo < b.lineNo ? -1 : 1));
        lineNos.forEach((tokens) => {
            tokens.tokens.sort((a, b) => a.column === b.column ? 0 : (a.column > b.column ? -1 : 1));
            tokens.tokens.forEach((token) => {
                const lineNo = tokens.lineNo - 1;
                if (rawLines[lineNo] === undefined) {
                    return console.error(`Couldnt find line no ${lineNo} in the raw lines (${rawLines.length})`);
                }

                rawLines[lineNo] = rawLines[lineNo]
                    .substring(0, token.column) + token.tag + rawLines[lineNo].substring(token.column);
            });
        });

        file.language = CodeLanguages(file.name);
        file.noLines = rawLines.length - 1;
        const highlighted = prism.highlight(rawLines.join("\n"), prism.languages[file.language], file.language);
        file.code = highlighted
            .replace(/\u0000/g, "<span class=\"code-coverage-highlight\" title=\"The statement has not been covered\">")
            .replace(/\u0001/g, "<span class=\"code-coverage-highlight\" title=\"The branch has not been covered\">")
            .replace(/\u0002/g, "<span class=\"code-coverage-highlight\" title=\"The function has not been covered\">")
            .replace(/\u0003/g, "</span>");

        return file;
    }

    /**
     * @todo
     */
    protected processStatement(statement, id: string, hash, file: ICodeCoverageFile): boolean {
        file.stats.statements.total++;
        if (hash > 0) {
            file.stats.statements.done++;
            return true;
        }

        this.addToken(statement.start.line, statement.start.column, "\u0000");
        this.addToken(statement.end.line, statement.end.column, "\u0003");
        return false;
    }

    /**
     * @todo
     */
    protected processBranch(branch, id: string, hash, file: ICodeCoverageFile): void {
        file.stats.branches.total += branch.locations.length;

        let failed = true;
        for (let i = 0, len = hash.length; i < len; i++) {
            if (hash[i] !== 0) {
                failed = false;
                break;
            }
        }

        if (!failed) {
            file.stats.branches.done += branch.locations.length;
        } else {
            this.addToken(branch.loc.start.line, branch.loc.start.column, "\u0001");
            this.addToken(branch.loc.end.line, branch.loc.end.column, "\u0003");
        }
    }

    /**
     * @todo
     */
    protected processFunction(fnc, id: string, hash, file: ICodeCoverageFile): void {
        file.stats.functions.total++;
        if (hash > 0) {
            file.stats.functions.done++;
        } else {
            this.addToken(fnc.decl.start.line, fnc.decl.start.column, "\u0002");
            this.addToken(fnc.decl.end.line, fnc.decl.end.column + 1, "\u0003");
        }
    }

    /**
     * @todo
     */
    protected addToken(line, col: number, char: string): void {
        line = parseInt(line, 10);
        let idx = this.tokens.findIndex((e) => e.lineNo === line);
        if (idx === -1) {
            idx = this.tokens.push({
                lineNo: parseInt(line, 10),
                tokens: [],
            }) - 1;
        }

        this.tokens[idx].tokens.push({
            column: col,
            tag: char,
        });
    }

    /** @inheritdoc */
    protected async doRender(): Promise<string> {
        const cfg: any = {};
        cfg.failPV = this.config.pctLevels && this.config.pctLevels.failedMax ?
            this.config.pctLevels.failedMax || 70 :
            70;

        cfg.passPV = this.config.pctLevels && this.config.pctLevels.passedMax ?
            this.config.pctLevels.passedMax || 100 :
            100;

        return await this.reporter.renderView(
            this.config.view || path.normalize(path.join(__dirname, "..", "..", "views", "CodeCoverage", "layout.pug")),
            { config: cfg, report: this.report },
        );
    }
}
