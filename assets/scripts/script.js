$(document).ready(function() {
    var $generated = $(".generated-on");
    $generated.text(moment($generated.text()).fromNow());

    $(".menu .item[data-tab]").tab({ onVisible: function() { window.scrollTo(0, 0); } });
    $(".ui.accordion").accordion({ exclusive: false, closeNested: false });
});

// $(".ui.menu .item[data-tab]").tab("change tab", "tab-id");

function renderChart(c) {
    var chart = new google.visualization.PieChart(document.getElementById("piechart-" + c.id));
    var options = {
        width: "100%",
        height: 200,
        is3D: true,
        fontSize: "13",
        fontName: '"Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif',
        slices: [],
        legend: "bottom"
    };

    var rows = [
        ["Label", "Value"]
    ];

    c.rows.forEach(function(row, idx) {
        rows.push([row.label, row.value]);
        options.slices[idx + 1] = { offset: 0.4 };
    });

    if (c.colors) {
        options.colors = c.colors;
    }

    chart.draw(google.visualization.arrayToDataTable(rows), options);
}